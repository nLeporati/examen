from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django import forms

class RegistroForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = {
            'username',
            'email',
            'password1',
            'password2',
            }
        order_fields={
            'username',
            'email',
            'password1',
            'password2',
            }
        labels = {
            'username':'Nombre de Usuario',
            'email':'Email',
            'password1':'Contraseña',
            'password2':'Repita Contraseña',
            }
        widgets = {
            'username' : forms.TextInput(attrs={'type': 'text','class':'form-control','required':''}),
            'email' : forms.TextInput(attrs={'type': 'email','class':'form-control','required':''}),
            'password1' : forms.TextInput(attrs={'type': 'password','class':'form-control','required':''}),
            'password2' : forms.TextInput(attrs={'type': 'password','class':'form-control','required':''}),
            }
        error_messages = {
            'required':'<font color="red">Campo Obligatorio',
        }

    def save(self, commit=True):
        user = super(RegistroForm, self).save(commit=False)
        user.username = self.cleaned_data["username"]
        user.email = self.cleaned_data["email"]
        user.password1 =self.cleaned_data["password1"]
        user.password2 =self.cleaned_data["password2"]
        if commit:
            user.save()
        return user