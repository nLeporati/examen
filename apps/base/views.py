from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.views.generic import CreateView
from django.urls import reverse_lazy
from .forms import RegistroForm
# Create your views here.

def home(request):
    return render(request, 'home.html')

class RegistroUsuario(CreateView):
    model = get_user_model()
    template_name = "registro.html"
    form_class = RegistroForm
    success_url = reverse_lazy('base:home')