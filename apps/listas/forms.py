from .models import Lista
from django import forms


class ListaForm(forms.ModelForm):

    class Meta:
        model=Lista
        fields = [
            'nombre',
            'costoTotalPresupuestado',
            # 'costoTotalReal',           
            'estado',          
        ]
        labels = {
            'nombre':'Nombre',
            # 'costoTotalReal':'Costo Total Real',
            'costoTotalPresupuestado':'Costo Total Presupuestado',
            'estado':'Estado',            
        }
        widgets = {
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            # 'costoTotalReal':forms.TextInput(attrs={'class':'form-control'}),
            'costoTotalPresupuestado':forms.TextInput(attrs={'class':'form-control'}),
            'estado':forms.Select(choices="LISTA_ESTADOS",attrs={'class':'form-control'}),
            # 'productos':forms.SelectMultiple(attrs={'class':'form-control'}),
        }